# Parallel Programming Project

The project focuses on parallelizing the sequential implementation of **K nearest neighbors (kNN)** algorithm - one of the basic supervised learning algorithm in Machine Learning, together with evaluating its performance based on some benchmark metrics.

**1. Motivation & Goals**
 - Machine Learning is a subfield of computer science that gives computers abilities to learn without being explicitly programmed. The two major problems of ML are Regression and Classification. Each problem comes up with different algorithms. This project discusses one of the basic supervised learning algorithm of Classification problem - K neareast neighbors (kNN)
 - Although kNN is quite simple and extremely efficient in some cases; however, in case the data are too big and consist of a great number of features, kNN might take a huge cost of computation for the distance between the instance data point to all training data points
 - Therefore, a parallel algorithm for kNN is obviously neccessary to solve the above disadvantage.

**2. Project Details**
 - The project will explain the kNN algorithm and its sequential implementation
 - Based on knowledge of the subject Parallel Programming, there are some parts such as **Decomposition, Aggregation, Communication and Mapping** in order to propose and build up a parallel implementation 
 - The parallel implementation will ground on different parallel programming models:
    + **Multithreading** with **CUDA** and the library **PyCuda**
    + **Multiprocessing** with **Message Passing** model +  the help of framework **MPI** via **MPICH** library
 - The programming language used to implement is **Python** and some of its powerful libraries like **pycuda**, **mpy4py**, **pandas**, **numpy**, **matplotlib**...
 - The performance evaluation benchmark matrics includes **Speedup** and **Effeciency** to
    + Compare parallel implementations to the sequential implementation
    + Also compare parallel implementations themselves

**3. Project Setup**

a.	MPI implementation: 

- Due to the limited resources, I could not get MPI implementation worked well in my physical machine, I therefore worked around by using a virtual machine which was created by Docker with environment setup. Here are steps:

- Install Docker/container: 
	+ Download and install Docker desktop application for MacOS 
	+ Using Docker to create a container named ‘mpimachine’ with Ubuntu platform

	docker run -it —name=mpimachine ubuntu:latest /bin/bash

- Start/Attach the container by docker to access into the container 'mpimachine'

- Update the machine 
	+ apt update

- Install ‘vim’ for editing resource files
	+ apt install vim

- Use python2.7 by default of Ubuntu machine
- Install MPICH 
	+ apt install mpich

- Install MPI for python:
	+ apt install python-pip
	+ pip install mpi4py

- Test with small program (helloworld.py)

	from mpi4py import MPI
	
	comm = MPI.COMM_WORLD
	
	rank = comm.Get_rank()
	
	size = comm.Get_size()
	
	print("hello world from process " +  str(rank) + " of " + str(size))

- Run the parallel ‘helloworld’ program with 5 processes simultaneously by the command ‘mpiexec’ or ‘mpirun’

	mpiexec -n 5 python hello.py 
   	
	mpirun -np 5 python hello.py
	
- Install other libraries: numpy, pandas, sklearn for python
	+ apt install python-numpy
	+ apt install python-pandas
	+ apt install python-sklearn python-sklearn-lib 

- Install GIT if unavailable, then use GIT to clone this project

- Access to the folder named 'parallel-programming-project/sourcecode'

- Run the file named 'KNN_Parallel_MPI.py' in a similar way to 'helloword' program, then see the result

b.	CUDA implementation: simply using Google Collabs to run .ipynb files

**4. (Optional) Test with a ready container**

- From the container 'mpimachine', I built an Docker image and push it it Docker hub under a repo
- The repo is available under the link below
	https://hub.docker.com/repository/docker/duyvu140893/mpimachine/general
- Follow the overview of this repo to get instruction for testing
